import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { Icon } from "react-native-elements";
import { Text, View } from "../components/Themed";
import React, { SetStateAction, useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import PokedexItem from "./PokedexItem";

/**
 ** Pokeapi data structure
 */
export interface Pokemon {
  id: string;
  name: string;
  checked: boolean;
  url: string;
}

/**
 ** Requests all pokemons in the API and map them
 * @param props
 * @returns Pokedex View
 */
export default function PokedexScreen(props: any) {
  const [pokemons, setPokemons] = useState([]);
  const [searchField, setSearchField] = useState("");
  const store = async (pokemons: any) => {
    try {
      const value = await AsyncStorage.getItem("@pokemons");
      console.log(value);
      if (value !== null) {
        // * Merge all checked states into a single result object
        JSON.parse(value).forEach((pokemon: Pokemon) => {
          pokemons.results.find(
            (pokemonFetched: Pokemon) => pokemon.id === pokemonFetched.id
          ).checked = pokemon.checked;
        });
      }
      await AsyncStorage.setItem("@pokemons", JSON.stringify(pokemons.results));
      setPokemons(pokemons.results);
      return true;
    } catch (e) {
      console.error("Error getting saved pokemons");
      setPokemons(pokemons.results);
      return false;
    }
  };

  useEffect(() => {
    fetchPokemons();
  }, []);
  const fetchPokemons = () => {
    fetch("https://pokeapi.co/api/v2/pokemon?limit=1500")
      .then((response) => response.json())
      .then((pokemons) => {
        pokemons.results.forEach((pokemon: Pokemon) => {
          const regexp = new RegExp(/\/(\d+)\//);
          const result = regexp.exec(pokemon.url);
          pokemon.checked = false;
          pokemon.id = Array.isArray(result) ? result[1] : "0";
        });
        store(pokemons).then((value: any) => {
          console.log(value);
        });
      });
  };

  const savePokemons = async (evt: any) => {
    try {
      await AsyncStorage.setItem("@pokemons", JSON.stringify(pokemons));
    } catch (e) {
      console.error("Error saving pokemons");
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.searchCont}>
        <Text style={styles.search}>Pokédex de {props.route.params.name}</Text>
        <TextInput
          style={styles.searchField}
          placeholder="Search Pokemons"
          onChangeText={(value: SetStateAction<string>) =>
            setSearchField(value)
          }
          value={searchField}
        />
      </View>
      <View
        style={{
          flex: 0.8,
          display: "flex",
          marginLeft: 10,
          marginRight: 10,
        }}
      >
        <FlatList
          contentContainerStyle={styles.container}
          numColumns={3}
          data={pokemons.filter((pokemon: Pokemon) =>
            pokemon.name.toLowerCase().includes(searchField.toLowerCase())
          )}
          renderItem={({ item }) => (
            <PokedexItem
              {...props}
              pokemon={item}
              index={item.id}
              onChecked={savePokemons}
            />
          )}
          keyExtractor={(item: Pokemon) => item.id}
        />
        {/* {pokemons
          .filter((pokemon: Pokemon) =>
            pokemon.name.toLowerCase().includes(searchField.toLowerCase())
          )
          .map((pokemon: Pokemon, index) => (
            <PokedexItem
              key={pokemon.id}
              {...props}
              pokemon={pokemon}
              index={pokemon.id}
              onChecked={savePokemons}
            />
          ))} */}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30,
  },
  searchCont: {
    display: "flex",
    flex: 0.2,
    justifyContent: "space-around",
    alignItems: "center",
    zIndex: 1,
    marginTop: 10,
  },
  search: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  searchField: {
    height: 40,
    borderWidth: 1,
    borderColor: "#000",
    textAlign: "center",
    width: 250,
    borderRadius: 50,
  },
  fillView: {
    position: "absolute",
    width: 10,
    height: 10,
    top: "25%",
    left: "25%",
    backgroundColor: "transparent",
  },
});
