import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  ActivityIndicator,
  TextInput,
  Button,
} from "react-native";
import { Snackbar } from "react-native-paper";

/**
 ** Login component for Pokedex
 * @param props
 * @returns Login View
 */

export default function PokelogScreen(props: any) {
  const [name, onChangeName] = React.useState("");
  const [visible, setVisible] = React.useState(false);

  const onDismissSnackBar = () => setVisible(false);

  return (
    <View style={{ flex: 1, alignItems: "center" }}>
      <Image
        style={styles.image}
        source={require("../assets/images/pokedex.png")}
      />
      <Text style={styles.text}>Bienvenue dans votre nouveau Pokédex</Text>
      <TextInput
        style={styles.input}
        onChangeText={onChangeName}
        value={name}
        placeholder="Veuillez entrer votre nom"
        keyboardType="default"
      />
      <Button
        title="Démarrer"
        onPress={() =>
          name ? (
            props.navigation.navigate("Mon Pokedex", {
              name,
            })
          ) : (
            <Snackbar visible={visible} onDismiss={onDismissSnackBar}>
              Veuillez remplir votre nom !
            </Snackbar>
          )
        }
      />
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 200,
    height: 200,
  },
  text: {
    fontSize: 22,
    marginBottom: 15,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
