import { Text, View } from "../components/Themed";
import React, { useState } from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements/dist/icons/Icon";
import { Image } from "react-native-elements/dist/image/Image";

/**
 ** List item for api results map
 * @param props
 * @returns List item View
 */
export default function PokedexItem(props: any) {
  const [isFilled, setIsFilled] = useState(props.pokemon.checked);
  const bgColor = props.pokemon.checked ? "#eef5deaa" : "white";

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      key={props.index}
      style={[styles.card, { backgroundColor: bgColor }]}
      onPress={() =>
        props.navigation.navigate("Details", {
          pokemon: props.pokemon.name,
        })
      }
    >
      <View style={styles.cardHeader}>
        <TouchableOpacity
          style={[styles.iconTouch, { backgroundColor: bgColor }]}
          onPress={() => {
            props.pokemon.checked = !props.pokemon.checked;
            setIsFilled(props.pokemon.checked);
            props.onChecked();
          }}
        >
          {isFilled ? (
            <Icon name="star" color="black" />
          ) : (
            <Icon name="star-border" color="black" />
          )}
        </TouchableOpacity>
      </View>
      <Image
        style={{ width: 100, height: 100 }}
        source={{
          uri: `https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/${props.pokemon.name}.png`,
        }}
      />
      <Text>{props.pokemon.name}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  card: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#dde4cdff",
    marginHorizontal: 10,
    marginVertical: 10,
    color: "#dde4cdff"
  },
  cardHeader: {
    display: "flex",
    justifyContent: "flex-end",
  },
  iconTouch: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
});
