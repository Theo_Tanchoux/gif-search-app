import React, { useState } from "react";
import {
  Modal,
  Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TextInput,
  View,
} from "react-native";
import {
  GiphyContent,
  GiphyGridView,
  GiphyMedia,
  GiphyMediaType,
  GiphySDK,
  GiphyVideoView,
} from "@giphy/react-native-sdk";
import { RootTabScreenProps } from "../types";
import { GIPHY_API_KEY } from "@env";
import { Icon, Text } from "react-native-elements";

// Configure API keys
GiphySDK.configure({
  apiKey: GIPHY_API_KEY,
});

export default function GifSearchScreen({
  navigation,
}: RootTabScreenProps<"GifSearch">) {
  const [searchQuery, setSearchQuery] = useState<string>("");
  const [media, setMedia] = useState<GiphyMedia | null>(null);

  return (
    <SafeAreaView style={styles.mainContainer}>
      <View style={styles.searchCont}>
        <TextInput
          style={styles.searchField}
          autoFocus
          onChangeText={setSearchQuery}
          placeholder="Search..."
          value={searchQuery}
        />
      </View>
      <GiphyGridView
        content={GiphyContent.search({
          searchQuery,
          mediaType: GiphyMediaType.Video,
        })}
        cellPadding={3}
        style={styles.gridView}
        onMediaSelect={(e) => {
          console.log(e.nativeEvent);
          setMedia(e.nativeEvent.media);
        }}
      />
      {media && (
        // TODO: Faire fonctionner la modal
        <Modal
          animationType="slide"
          transparent={true}
          visible={media !== null}
          onRequestClose={() => {
            console.log(media);
            setMedia(null);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Pressable onPress={() => setMedia(null)}>
                <Icon name="close" />
              </Pressable>
              <GiphyVideoView
                media={media}
                autoPlay={true}
                style={{ aspectRatio: media.aspectRatio }}
              />
            </View>
          </View>
        </Modal>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    width: "100%",
  },
  searchCont: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 1,
  },
  searchField: {
    display: "flex",
    height: 40,
    borderWidth: 1,
    borderColor: "#000",
    textAlign: "center",
    width: 250,
    borderRadius: 50,
  },
  centeredView: {
    flex: 1,
    display: "flex",
    justifyContent: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  gridView: {
    display: "flex",
    flex: 0.9,
    width: "100%",
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
