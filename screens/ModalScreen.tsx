import { GiphyMedia, GiphyVideoView } from "@giphy/react-native-sdk";
import { StatusBar } from "expo-status-bar";
import React from "react";
import { Platform, ScrollView, StyleSheet } from "react-native";
import { RouteProp } from "@react-navigation/native";

import EditScreenInfo from "../components/EditScreenInfo";
import { Text, View } from "../components/Themed";

export default function ModalScreen(
  route: RouteProp<{ params: { media: GiphyMedia } }, "params">
) {
  const { media } = route.params;
  return (
    // <View style={styles.centeredView}>
    //   <ScrollView style={styles.modalView}>
    <GiphyVideoView
      media={media}
      autoPlay={true}
      style={{ aspectRatio: media.aspectRatio }}
    />
    //   </ScrollView>
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
