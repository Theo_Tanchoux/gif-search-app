# GIF and Pokemon app

## Installation

[Node.js](https://nodejs.org/) latest version required.

Install the dependencies and start the app.

### iOS

```sh
npm i
expo run:ios 
```

### Android

```sh
npm i
expo run:android 
```
