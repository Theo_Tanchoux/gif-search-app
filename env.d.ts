declare module "@env" {
  export const GIPHY_API_KEY: string;
  export const ENV: "dev" | "prod";
}
